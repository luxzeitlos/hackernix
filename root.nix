{ nixpkgs, ... }:
{
  virtualisation.rosetta.enable = true;

  nix.settings.experimental-features = [ "nix-command" "flakes" ];
  nix.nixPath = [
    "nixpkgs=/etc/nixpkgs"
  ];
  environment.etc."nixpkgs".source = "${nixpkgs}";

  boot.growPartition = true;

  time.timeZone = "Europe/Berlin";
  i18n = {
    defaultLocale = "en_US.UTF-8";
    extraLocaleSettings = {
      "LC_TIME" = "de_CH.UTF-8";
    };
  };
  console = {
    keyMap = "de_CH-latin1";
  };

  nixpkgs.config.allowUnfree = true;
  
  environment.systemPackages = with pkgs; [
    vim
    wget
    htop
    git
    curl
    tree
    bmon
    httpie
    inetutils
    kubectl
    ranger
    dnsutils
    bmon
    tmux
  ];

  services.openssh = {
    enable = true;
    openFirewall = true;
  };

  system.stateVersion = "23.05";
}