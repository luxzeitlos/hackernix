{
  description = "A very basic flake";
  inputs.nixpkgs.url = "nixpkgs/nixos-23.05";

  outputs = { self, nixpkgs }@inputs: {
    nixosConfigurations.aarm64-linux.default = nixpkgs.lib.nixosSystem {
      system = "aarm64-linux";
      specialArgs = inputs;
      modules = [
        "${nixpkgs}/nixos/modules/virtualisation/rosetta.nix"
        ./root.nix
      ];
    };
  };
}
